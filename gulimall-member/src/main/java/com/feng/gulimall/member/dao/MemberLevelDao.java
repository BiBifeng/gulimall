package com.feng.gulimall.member.dao;

import com.feng.gulimall.member.entity.MemberLevelEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员等级
 * 
 * @author 丰
 * @email 1507298022@qq.com
 * @date 2022-10-21 21:59:11
 */
@Mapper
public interface MemberLevelDao extends BaseMapper<MemberLevelEntity> {
	
}
