package com.feng.gulimall.member.controller;

import java.util.Arrays;
import java.util.Map;

import com.feng.gulimall.member.feign.CouponFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.feng.gulimall.member.entity.GrowthChangeHistoryEntity;
import com.feng.gulimall.member.service.GrowthChangeHistoryService;
import com.feng.common.utils.PageUtils;
import com.feng.common.utils.R;



/**
 * 成长值变化历史记录
 *
 * @author 丰
 * @email 1507298022@qq.com
 * @date 2022-10-21 21:59:12
 */
@RestController
@RequestMapping("member/growthchangehistory")
public class GrowthChangeHistoryController {
    @Autowired
    private GrowthChangeHistoryService growthChangeHistoryService;

    @Autowired
    private CouponFeign couponFeign;     // 远程调用接口

    // 测试远程调用，coupon服务
    @GetMapping("/test")
    public R test(){
        GrowthChangeHistoryEntity changeHistoryEntity = new GrowthChangeHistoryEntity();
        changeHistoryEntity.setChangeCount(10000);
        return R.ok().put("test2", changeHistoryEntity)
                .put("test1", couponFeign.test());
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = growthChangeHistoryService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		GrowthChangeHistoryEntity growthChangeHistory = growthChangeHistoryService.getById(id);

        return R.ok().put("growthChangeHistory", growthChangeHistory);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody GrowthChangeHistoryEntity growthChangeHistory){
		growthChangeHistoryService.save(growthChangeHistory);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody GrowthChangeHistoryEntity growthChangeHistory){
		growthChangeHistoryService.updateById(growthChangeHistory);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		growthChangeHistoryService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
