package com.feng.gulimall.member.feign;

import com.feng.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @projectName: gulimall
 * @package: com.feng.gulimall.member.feign
 * @className: CouponFeign
 * @author: 丰
 * @description:
 * @date: 2022/10/22 9:51
 * @version: 1.0
 */
@FeignClient("gulimall-coupon")
public interface CouponFeign {

    @GetMapping("coupon/coupon/test")
    public R test();
}
