package com.feng.common.exception;

/**
 * @projectName: gulimall
 * @package: com.feng.common.exception
 * @className: BizCodeEnum
 * @author: 丰
 * @description:
 * @date: 2022/11/3 10:05
 * @version: 1.0
 */
public enum BizCodeEnum {
    UNKNOWN_EXCEPTION(10000, "系统未知异常"),
    VALID_EXCEPTION(10001, "参数格式校验失败");

    private Integer code;
    private String msg;

    BizCodeEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
