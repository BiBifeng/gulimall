package com.feng.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @description: 服务调用的传输数据
 * @author 丰
 * @date 2022/11/11 15:55
 * @version 1.0
 */
@Data
public class SpuBoundsTo {
    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;

}
