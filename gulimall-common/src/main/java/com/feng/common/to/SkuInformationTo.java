package com.feng.common.to;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @description: TODO
 * @author 丰
 * @date 2022/11/11 16:09
 * @version 1.0
 */
@Data
public class SkuInformationTo {
    private Long skuId;
    // sku的优惠等信息
    // sku的折扣信息  sms_sku_ladder
    private int fullCount;
    private BigDecimal discount;    // 折扣
    private int countStatus;
    // sku的满减信息  sms_sku_full_reduction
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;
    // 会员价格信息  sms_member_price
    private List<MemberPrice> memberPrice;
}
