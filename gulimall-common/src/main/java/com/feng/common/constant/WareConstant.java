package com.feng.common.constant;
/**
 * @description: 常量
 * @author 丰
 * @date 2022/11/17 22:06
 * @version 1.0
 */
public class WareConstant {
    public enum PurchaseStatusEnum{
        CREATED(0, "新建"),
        ASSIGNMENT(1, "已分配"),
        ACCEPT(2, "已领取"),
        FINISH(3, "已完成"),
        HASERRO(4, "有异常");

        private int code;
        private String msg;

        PurchaseStatusEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public Integer getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }

    public enum PurchaseItemStatusEnum{
        CREATED(0, "新建"),
        ASSIGNMENT(1, "已分配"),
        ACCEPT(2, "正在采购"),
        FINISH(3, "已完成"),
        HASERRO(4, "采购失败");

        private int code;
        private String msg;

        PurchaseItemStatusEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public Integer getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }

}
