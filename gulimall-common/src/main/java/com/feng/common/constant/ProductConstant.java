package com.feng.common.constant;

/**
 * @description: product模块的常量信息
 * @author 丰
 * @date 2022/11/8 9:20
 * @version 1.0
 */
public class ProductConstant {
    public enum AttrEnum{
        ATTR_TYPE_SALE(0, "销售属性"),
        ATTR_TYPE_BASE(1, "基本属性");
        private int code;
        private String msg;

        AttrEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public Integer getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }
    }
}
