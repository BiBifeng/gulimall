package com.feng.common.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * @projectName: gulimall
 * @package: com.feng.common.validator
 * @className: ListValue
 * @author: 丰
 * @description:
 * @date: 2022/11/3 16:54
 * @version: 1.0
 */

@Documented
@Constraint(validatedBy = { ListValueConstraintValidator.class })   // 使用哪个校验器校验
// 用在什么地方
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)    // 可以在运行时获取到
public @interface ListValue {
    String message() default "{com.feng.common.validator.ListValue.message}";   // 指定错误信息,从配置文件中取

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    int[] value() default {};
}
