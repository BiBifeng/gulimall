package com.feng.common.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Set;
import java.util.TreeSet;

/**
 * @description: 自定义的校验器， 实现校验器，第一个注解：校验注解，第二个：校验的类型
 * @author 丰
 * @date 2022/11/3 17:11
 * @version 1.0
 */
public class ListValueConstraintValidator implements ConstraintValidator<ListValue, Integer> {

    Set<Integer> set = new TreeSet<>();
    // 初始化，获取ListValue中的值
    @Override
    public void initialize(ListValue constraintAnnotation) {
        // 获取值
        int[] ints = constraintAnnotation.value();
        for (int anInt : ints) {
            set.add(anInt);
        }
    }

    // 判断是否校验成功， 想要校验的值，
    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        // 如果是 ListValue 指定的值，那就校验成功
        return set.contains(integer);
    }
}
