package com.feng.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feng.common.to.SkuInformationTo;
import com.feng.common.utils.PageUtils;
import com.feng.gulimall.coupon.entity.SkuLadderEntity;

import java.util.Map;

/**
 * 商品阶梯价格
 *
 * @author 丰
 * @email 1507298022@qq.com
 * @date 2022-10-21 21:53:49
 */
public interface SkuLadderService extends IService<SkuLadderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveInfo(SkuInformationTo skuInformationTo);
}

