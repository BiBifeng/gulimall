package com.feng.gulimall.coupon.service.impl;

import com.feng.common.to.SkuInformationTo;
import com.feng.gulimall.coupon.entity.MemberPriceEntity;
import com.feng.gulimall.coupon.entity.SkuFullReductionEntity;
import com.feng.gulimall.coupon.service.MemberPriceService;
import com.feng.gulimall.coupon.service.SkuFullReductionService;
import com.google.j2objc.annotations.AutoreleasePool;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feng.common.utils.PageUtils;
import com.feng.common.utils.Query;

import com.feng.gulimall.coupon.dao.SkuLadderDao;
import com.feng.gulimall.coupon.entity.SkuLadderEntity;
import com.feng.gulimall.coupon.service.SkuLadderService;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.OverridesAttribute;


@Service("skuLadderService")
public class SkuLadderServiceImpl extends ServiceImpl<SkuLadderDao, SkuLadderEntity> implements SkuLadderService {

    // sku的满减信息  sms_sku_full_reduction

    // 会员价格信息  sms_member_price
    @Autowired
    private SkuFullReductionService skuFullReductionService;
    @Autowired
    private MemberPriceService memberPriceService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuLadderEntity> page = this.page(
                new Query<SkuLadderEntity>().getPage(params),
                new QueryWrapper<SkuLadderEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    @Transactional
    public void saveInfo(SkuInformationTo skuInformationTo) {
        // sku的折扣信息  sms_sku_ladder
        if (skuInformationTo.getFullCount() > 0 && skuInformationTo.getDiscount().compareTo(new BigDecimal(0)) > 0){
            SkuLadderEntity skuLadderEntity = new SkuLadderEntity();
            BeanUtils.copyProperties(skuInformationTo, skuLadderEntity);
            skuLadderEntity.setAddOther(skuInformationTo.getCountStatus());
            baseMapper.insert(skuLadderEntity);
        }

        // sku的满减信息  sms_sku_full_reduction
        if (skuInformationTo.getReducePrice().compareTo(new BigDecimal(0)) > 0
                && skuInformationTo.getFullPrice().compareTo(new BigDecimal(0)) > 0){
            SkuFullReductionEntity skuFullReductionEntity = new SkuFullReductionEntity();
            BeanUtils.copyProperties(skuInformationTo, skuFullReductionEntity);
            skuFullReductionEntity.setAddOther(skuInformationTo.getPriceStatus());
            skuFullReductionService.save(skuFullReductionEntity);
        }


        // 会员价格信息  sms_member_price
        List<MemberPriceEntity> collect = skuInformationTo.getMemberPrice().stream().map((price) -> {
            MemberPriceEntity memberPriceEntity = new MemberPriceEntity();
            memberPriceEntity.setMemberPrice(price.getPrice());
            memberPriceEntity.setAddOther(1);
            memberPriceEntity.setMemberLevelId(price.getId());
            memberPriceEntity.setSkuId(skuInformationTo.getSkuId());
            memberPriceEntity.setMemberLevelName(price.getName());
            return memberPriceEntity;
        }).filter((entity) -> {
            return entity.getMemberPrice().compareTo(new BigDecimal(0)) > 0;
        }).collect(Collectors.toList());
        memberPriceService.saveBatch(collect);
    }

}
