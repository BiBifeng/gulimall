package com.feng.gulimall.coupon.dao;

import com.feng.gulimall.coupon.entity.HomeSubjectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 专题商品
 * 
 * @author 丰
 * @email 1507298022@qq.com
 * @date 2022-10-21 21:53:51
 */
@Mapper
public interface HomeSubjectSpuDao extends BaseMapper<HomeSubjectSpuEntity> {
	
}
