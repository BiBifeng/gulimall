package com.feng.gulimall.product;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;


/**
 * JSR303数据校验
 *  导入依赖 spring-boot-starter-validation
 *  给Bean添加注解  @NotBlank(message = "品牌名必须提交")
 *  在controller里加上 @Valid注解
 *  给检验的Bean后面跟上一个 @BandingResult，得到校验之后的结果
 *  分组校验(groups)
 *      @NotBlank(message = "品牌名必须提交", groups = {UpdateGroup.class, AddGroup.class})
 *      将 @Valid 替换成 @Validated(AddGroup.class) 指定哪些组生效，
 *  自定义校验
 *      编写一个自定义的注解
 *      编写一个自定义的校验器  ConstraintValidator<ListValue, Integer>
 *      关联自定义的注解和校验器 @Constraint(validatedBy = { ListValueConstraintValidator.class })   // 使用哪个校验器校验
 *
 * 统一异常处理 @ControllerAdvice
 *  编写异常处理类，加上 @ControllerAdvice 注解
 *  使用 @ExceptionHandler 标注处理的异常类型
 */

@SpringBootApplication
@MapperScan("com.feng.gulimall.product.dao")    // map扫描
@EnableDiscoveryClient
@EnableFeignClients(basePackages = "com.feng.gulimall.product.feign")    // 开启远程调用，接口扫描
public class GulimallProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(GulimallProductApplication.class, args);
    }

}
