package com.feng.gulimall.product.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.feng.common.validator.group.AddGroup;
import com.feng.common.validator.group.UpdateGroup;
import com.feng.common.validator.group.UpdateStatusGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.feng.gulimall.product.entity.BrandEntity;
import com.feng.gulimall.product.service.BrandService;
import com.feng.common.utils.PageUtils;
import com.feng.common.utils.R;

import javax.validation.Valid;


/**
 * 品牌
 *
 * @author 丰
 * @email sunlightcs@gmail.com
 * @date 2022-10-21 19:58:58
 */
@RestController
@RequestMapping("product/brand")
public class BrandController {
    @Autowired
    private BrandService brandService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = brandService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{brandId}")
    public R info(@PathVariable("brandId") Long brandId){
		BrandEntity brand = brandService.getById(brandId);

        return R.ok().put("brand", brand);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@Validated(AddGroup.class) @RequestBody BrandEntity brand /*BindingResult result*/){
        // BindingResult  错误之后，封装错误信息
//        if (result.hasErrors()){
//            Map<String, Object> map = new HashMap<>();
//            result.getFieldErrors().forEach((item) -> {
//                // 获取错误信息
//                String message = item.getDefaultMessage();
//                // 获取错误字段
//                String name = item.getField();
//                map.put(name, message);
//            });
//            return R.error(400, "提交的数据不合法").put("data", map);
//        }else {
        brandService.save(brand);
//        }
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@Validated(UpdateGroup.class) @RequestBody BrandEntity brand){
        // 修改品牌时，要更新有关联的 CategoryBrandRelation 表
		brandService.updateDetal(brand);

        return R.ok();
    }

    /**
     * 修改状态  showStatus
     */
    @RequestMapping("/update/status")
    public R updateStatus(@Validated(UpdateStatusGroup.class) @RequestBody BrandEntity brand){
		brandService.updateById(brand);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] brandIds){
		brandService.removeByIds(Arrays.asList(brandIds));

        return R.ok();
    }

}
