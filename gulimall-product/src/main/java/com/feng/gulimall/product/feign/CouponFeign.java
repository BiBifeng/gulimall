package com.feng.gulimall.product.feign;

import com.feng.common.to.SkuInformationTo;
import com.feng.common.to.SpuBoundsTo;
import com.feng.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @description: TODO
 * @author 丰
 * @date 2022/11/11 15:49
 * @version 1.0
 */
@FeignClient("gulimall-coupon")
public interface CouponFeign {
    @RequestMapping("/coupon/spubounds/save")
    R saveBounds(@RequestBody SpuBoundsTo spuBoundsTo);

    @PostMapping("/coupon/skuladder/saveInfo")
    R saveInfo(@RequestBody SkuInformationTo skuInformationTo);
}
