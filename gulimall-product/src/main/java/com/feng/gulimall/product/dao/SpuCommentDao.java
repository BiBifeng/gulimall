package com.feng.gulimall.product.dao;

import com.feng.gulimall.product.entity.SpuCommentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品评价
 * 
 * @author 丰
 * @email sunlightcs@gmail.com
 * @date 2022-10-21 19:58:57
 */
@Mapper
public interface SpuCommentDao extends BaseMapper<SpuCommentEntity> {
	
}
