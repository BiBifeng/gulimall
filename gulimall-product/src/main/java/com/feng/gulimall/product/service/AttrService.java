package com.feng.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feng.common.utils.PageUtils;
import com.feng.gulimall.product.entity.AttrEntity;
import com.feng.gulimall.product.vo.AttrRespVo;
import com.feng.gulimall.product.vo.AttrVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author 丰
 * @email sunlightcs@gmail.com
 * @date 2022-10-21 19:58:59
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPage(Map<String, Object> params, Long catelogId, String attrType);

    void saveDetial(AttrVo attrVo);

    AttrRespVo getAttrInfo(Long attrId);

    void updateAttr(AttrVo attrVo);

    void removeAttrs(Long[] attrIds);

    List<AttrEntity> getRelationAttr(Long attrGroupId);

    PageUtils getAttrNoRelation(Map<String, Object> params, Long attrGroupId);
}

