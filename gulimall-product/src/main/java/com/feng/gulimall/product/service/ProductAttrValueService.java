package com.feng.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feng.common.utils.PageUtils;
import com.feng.gulimall.product.entity.ProductAttrValueEntity;
import com.feng.gulimall.product.vo.save.BaseAttrs;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author 丰
 * @email sunlightcs@gmail.com
 * @date 2022-10-21 19:58:57
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttrValue(Long spuId, List<BaseAttrs> baseAttrs);
}

