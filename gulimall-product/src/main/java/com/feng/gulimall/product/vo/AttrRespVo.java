package com.feng.gulimall.product.vo;

import lombok.Data;

/**
 * @description: TODO
 * @author 丰
 * @date 2022/11/7 15:15
 * @version 1.0
 */
@Data
public class AttrRespVo extends AttrVo{
    private String groupName;
    private String catelogName;
    private Long[] catelogPath;
}
