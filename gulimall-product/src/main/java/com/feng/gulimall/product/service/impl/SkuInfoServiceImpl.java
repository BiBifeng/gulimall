package com.feng.gulimall.product.service.impl;

import com.alibaba.nacos.api.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mysql.cj.x.protobuf.MysqlxDatatypes;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feng.common.utils.PageUtils;
import com.feng.common.utils.Query;

import com.feng.gulimall.product.dao.SkuInfoDao;
import com.feng.gulimall.product.entity.SkuInfoEntity;
import com.feng.gulimall.product.service.SkuInfoService;


@Service("skuInfoService")
public class SkuInfoServiceImpl extends ServiceImpl<SkuInfoDao, SkuInfoEntity> implements SkuInfoService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                new QueryWrapper<SkuInfoEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPageByCondetion(Map<String, Object> params) {

        /**
         * key:
         * catelogId: 0
         * brandId:
         * min:
         * max:
         */
        String key = (String) params.get("key");
        String catelogId = (String) params.get("catelogId");
        String brandId = (String) params.get("brandId");
        String min = (String) params.get("min");
        String max = (String) params.get("max");
        LambdaQueryWrapper<SkuInfoEntity> wrapper = new LambdaQueryWrapper<>();

        if (!StringUtils.isBlank(key)){
            wrapper.and((w) -> {
                w.eq(SkuInfoEntity::getSkuId, key).or().like(SkuInfoEntity::getSkuName, key);
            });
        }
        if (!StringUtils.isBlank(catelogId) && !"0".equals(catelogId)){
            wrapper.eq(SkuInfoEntity::getCatalogId, catelogId);
        }

        if (!StringUtils.isBlank(brandId) && !"0".equals(brandId)){
            wrapper.eq(SkuInfoEntity::getBrandId, brandId);
        }

        if (!StringUtils.isBlank(min)){
            wrapper.ge(SkuInfoEntity::getPrice, min);
        }

        if (!StringUtils.isBlank(max) && !"0".equals(max)){
            wrapper.le(SkuInfoEntity::getPrice, max);
        }

        IPage<SkuInfoEntity> page = this.page(
                new Query<SkuInfoEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);

    }

}
