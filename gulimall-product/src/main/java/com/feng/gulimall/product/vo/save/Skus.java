/**
  * Copyright 2022 bejson.com
  */
package com.feng.gulimall.product.vo.save;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Auto-generated: 2022-11-11 13:15:16
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Skus {

    // sku的销售属性信息  pms_sku_sale_attr_value
    private List<Attr> attr;
    // sku的基本信息  pms_sku_info
    private String skuName;
    private BigDecimal price;
    private String skuTitle;
    private String skuSubtitle;

    // sku的图片信息  pms_sku_images
    private List<Images> images;
    // sku的描述信息
    private List<String> descar;

    // sku的优惠等信息
    // sku的折扣信息  sms_sku_ladder
    private int fullCount;
    private BigDecimal discount;    // 折扣
    private int countStatus;
    // sku的满减信息  sms_sku_full_reduction
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;
    // 会员价格信息  sms_member_price
    private List<MemberPrice> memberPrice;

}
