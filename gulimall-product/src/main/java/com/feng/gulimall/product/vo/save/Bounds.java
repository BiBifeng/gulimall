/**
  * Copyright 2022 bejson.com
  */
package com.feng.gulimall.product.vo.save;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Auto-generated: 2022-11-11 13:15:16
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
// 积分表
@Data
public class Bounds {

    private BigDecimal buyBounds;
    private BigDecimal growBounds;

}
