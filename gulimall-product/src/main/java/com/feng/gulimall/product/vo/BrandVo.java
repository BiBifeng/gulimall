package com.feng.gulimall.product.vo;

import lombok.Data;

/**
 * @description: TODO
 * @author 丰
 * @date 2022/11/8 22:18
 * @version 1.0
 */
@Data
public class BrandVo {
    private Long brandId;
    private String brandName;

}
