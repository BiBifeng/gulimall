package com.feng.gulimall.product.dao;

import com.feng.gulimall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author 丰
 * @email sunlightcs@gmail.com
 * @date 2022-10-21 19:58:59
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
