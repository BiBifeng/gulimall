/**
  * Copyright 2022 bejson.com
  */
package com.feng.gulimall.product.vo.save;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * Auto-generated: 2022-11-11 13:15:16
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class SpuSaveVo {

    // spu的基本信息  pms_spu_info
    private String spuName;
    private String spuDescription;
    private Long catalogId;
    private Long brandId;
    private BigDecimal weight;
    private int publishStatus;

    // spu的描述信息  pms_spu_info_desc
    private List<String> decript;
    // spu的图像信息  pms_spu_images
    private List<String> images;
    // spu的积分信息  sms_spu_bounds
    private Bounds bounds;
    // spu的规格参数  pms_product_attr_value
    private List<BaseAttrs> baseAttrs;
    // spu的sku信息  sms_spu_bounds
    private List<Skus> skus;

}
