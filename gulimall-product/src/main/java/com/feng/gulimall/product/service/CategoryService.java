package com.feng.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feng.common.utils.PageUtils;
import com.feng.gulimall.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author 丰
 * @email sunlightcs@gmail.com
 * @date 2022-10-21 19:58:58
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    // 三级分类
    List<CategoryEntity> queryListTree();

    // 批量删除
    void deleteByIds(List<Long> asList);

    Long[] getCatelogIds(Long attrGroupId);

    void updateDetail(CategoryEntity category);
}

