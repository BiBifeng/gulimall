package com.feng.gulimall.product.controller;

import java.awt.*;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.feng.gulimall.product.vo.BrandVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.feng.gulimall.product.entity.CategoryBrandRelationEntity;
import com.feng.gulimall.product.service.CategoryBrandRelationService;
import com.feng.common.utils.PageUtils;
import com.feng.common.utils.R;



/**
 * 品牌分类关联
 *
 * @author 丰
 * @email sunlightcs@gmail.com
 * @date 2022-10-21 19:58:59
 */
@RestController
@RequestMapping("product/categorybrandrelation")
public class CategoryBrandRelationController {
    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    /**
     * 列表
     */
    @GetMapping("/catelog/list")
    public R catelogList(@RequestParam("brandId") Long brandId){
        System.out.println(brandId);
        LambdaQueryWrapper<CategoryBrandRelationEntity> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CategoryBrandRelationEntity::getBrandId, brandId);
        List<CategoryBrandRelationEntity> list = categoryBrandRelationService.list(wrapper);

        return R.ok().put("data", list);
    }

    // http://localhost:88/api/product/categorybrandrelation/brands/list?t=1667916709996&catId=225
    @GetMapping("/brands/list")
    public R brandsList(Long catId){
        List<CategoryBrandRelationEntity> list = categoryBrandRelationService.getBaseMapper().selectList(new LambdaQueryWrapper<CategoryBrandRelationEntity>()
                .eq(CategoryBrandRelationEntity::getCatelogId, catId));
        List<BrandVo> data = list.stream().map((categoryBrandRelation) -> {
            BrandVo brandVo = new BrandVo();
            BeanUtils.copyProperties(categoryBrandRelation, brandVo);
            return brandVo;
        }).collect(Collectors.toList());
        return R.ok().put("data", data);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
		CategoryBrandRelationEntity categoryBrandRelation = categoryBrandRelationService.getById(id);

        return R.ok().put("categoryBrandRelation", categoryBrandRelation);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody CategoryBrandRelationEntity categoryBrandRelation){
//        categoryBrandRelationService.save(categoryBrandRelation);
        categoryBrandRelationService.saveCatelogAndBrand(categoryBrandRelation);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody CategoryBrandRelationEntity categoryBrandRelation){
		categoryBrandRelationService.updateById(categoryBrandRelation);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
		categoryBrandRelationService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
