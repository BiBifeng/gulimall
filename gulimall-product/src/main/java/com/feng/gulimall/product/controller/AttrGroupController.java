package com.feng.gulimall.product.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.feng.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.feng.gulimall.product.entity.AttrEntity;
import com.feng.gulimall.product.entity.CategoryEntity;
import com.feng.gulimall.product.service.AttrAttrgroupRelationService;
import com.feng.gulimall.product.service.AttrService;
import com.feng.gulimall.product.service.CategoryService;
import com.feng.gulimall.product.vo.AttrGroupWithAttrsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.feng.gulimall.product.entity.AttrGroupEntity;
import com.feng.gulimall.product.service.AttrGroupService;
import com.feng.common.utils.PageUtils;
import com.feng.common.utils.R;



/**
 * 属性分组
 *
 * @author 丰
 * @email sunlightcs@gmail.com
 * @date 2022-10-21 19:58:58
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrService attrService;

    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;

    // http://localhost:88/api/product/attrgroup/2/attr/relation
    // 查询展示关联数据
    @GetMapping("/{attrGroupId}/attr/relation")
    public R attrRelation(@PathVariable("attrGroupId") Long attrGroupId){
        List<AttrEntity> attrEntitys = attrService.getRelationAttr(attrGroupId);
        return R.ok().put("data", attrEntitys);
    }

    // http://localhost:88/api/product/attrgroup/attr/relation/delete
    // 移除属性分组与属性的关联关系
    @PostMapping("/attr/relation/delete")
    public R attrRelationDelete(@RequestBody AttrAttrgroupRelationEntity[] attrAttrgroupRelationEntitys){
        attrAttrgroupRelationService.removeAttrRelation(attrAttrgroupRelationEntitys);
        return R.ok();
    }

    // http://localhost:88/api/product/attrgroup/1/noattr/relation
    // 查询没有关联的属性
    @GetMapping("/{attrGroupId}/noattr/relation")
    public R noattrRelation(@PathVariable("attrGroupId") Long attrGroupId,
                            @RequestParam Map<String, Object> params){
        PageUtils page = attrService.getAttrNoRelation(params, attrGroupId);
        return R.ok().put("page", page);
    }

    // http://localhost:88/api/product/attrgroup/attr/relation
    // 添加属性分组关联
    @PostMapping("/attr/relation")
    public R addAttrRelation(@RequestBody AttrAttrgroupRelationEntity[] attrAttrgroupRelationEntitys){
        attrAttrgroupRelationService.addAttrRelation(attrAttrgroupRelationEntitys);
        return R.ok();
    }

    // 获取分类下所有分组和关联属性
    // http://localhost:88/api/product/attrgroup/225/withattr?t=1667918026955
    @GetMapping("/{catelogId}/withattr")
    public R getAttrGroupWithAttr(@PathVariable("catelogId") Long catelogId){
        List<AttrGroupWithAttrsVo> list = attrGroupService.getAttrGroupWithAttrs(catelogId);
        return R.ok().put("data", list);

    }


    /**
     * 列表
     */
    @RequestMapping("/list/{catelogId}")
    public R list(@RequestParam Map<String, Object> params,
                  @PathVariable("catelogId") Long catelogId){
//        PageUtils page = attrGroupService.queryPage(params);
        PageUtils page = attrGroupService.queryPage(params, catelogId);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);

        Long[] catelogIds = categoryService.getCatelogIds(attrGroup.getCatelogId());
        attrGroup.setCatelogPath(catelogIds);
        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
