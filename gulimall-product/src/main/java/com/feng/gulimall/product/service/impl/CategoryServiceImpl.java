package com.feng.gulimall.product.service.impl;

import com.alibaba.nacos.api.utils.StringUtils;
import com.feng.gulimall.product.service.CategoryBrandRelationService;
import com.sun.xml.internal.bind.v2.TODO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feng.common.utils.PageUtils;
import com.feng.common.utils.Query;

import com.feng.gulimall.product.dao.CategoryDao;
import com.feng.gulimall.product.entity.CategoryEntity;
import com.feng.gulimall.product.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> queryListTree() {
        // 查询出所有的数据
        List<CategoryEntity> categoryEntities = baseMapper.selectList(null);
        // 筛选分类
        // 获取一级分类
        List<CategoryEntity> categoryEntities1 = categoryEntities.stream().filter(item -> {
            return item.getParentCid() == 0;
        }).map(item -> {
            // 设置二三级分类
            setCategoryTwo(item, categoryEntities, 2);
            return item;
            // 从小到大 排序
        }).sorted((menu1, menu2) -> {
            return (menu1.getSort() == null ? 0: menu1.getSort()) - (menu2.getSort() == null ? 0: menu2.getSort());
        }).collect(Collectors.toList());    // 过滤出来的数据添加到categoryEntities1中
        return categoryEntities1;
    }

    @Override
    public void deleteByIds(List<Long> asList) {
        // TODO: 删除之前需要判断当前元素是否被引用
        baseMapper.deleteBatchIds(asList);
    }

    @Override
    public Long[] getCatelogIds(Long attrGroupId) {
        List<Long> list = new ArrayList<>();
        // 递归查父id
        getParentCatId(attrGroupId, list);
        Collections.reverse(list);
        return list.toArray(new Long[list.size()]);
    }

    @Override
    @Transactional
    public void updateDetail(CategoryEntity category) {
        baseMapper.updateById(category);
        categoryBrandRelationService.updateCategoryName(category.getCatId(), category.getName());

    }

    private void getParentCatId(Long attrGroupId, List<Long> list) {
        list.add(attrGroupId);
        CategoryEntity categoryEntity = baseMapper.selectById(attrGroupId);
        Long parentCid = categoryEntity.getParentCid();
        if (parentCid != 0){
            getParentCatId(parentCid, list);
        }

    }

    /**
     * @Author 丰
     * @Description 封装二三级分类
     * @Date 15:01 2022/10/22
     * @Param item：一级分类的一个实体。 categoryEntities： 所有数据
     */
    private void setCategoryTwo(CategoryEntity item, List<CategoryEntity> categoryEntities, int catLevel) {
        // 拿到二级分类的所有数据   如果表中存在一个分级字段
//        List<CategoryEntity> categoryEntities2 = categoryEntities.stream().filter(i -> {
//            return i.getCatLevel() == catLevel;     // 如果表中存在一个分级字段
//        }).filter(categoryEntity -> {
//            return item.getCatId().equals(categoryEntity.getParentCid());
//        }).sorted((menu1, menu2) -> {
//            return (menu1.getSort() == null ? 0: menu1.getSort()) - (menu2.getSort() == null ? 0: menu2.getSort());
//        }).collect(Collectors.toList());
//        // set到实体类中
//        item.setChildren(categoryEntities2);
//        // 递归查找
//        categoryEntities2.stream().forEach(categoryEntity -> {
//            setCategoryTwo(categoryEntity, categoryEntities, 3);
//        });

        //  如果不存在分级字段
        List<CategoryEntity> categoryEntities2 = categoryEntities.stream().filter(i -> {
            return item.getCatId().equals(i.getParentCid());
        }).map(categoryEntity -> {
            setCategoryTwo(categoryEntity, categoryEntities, 3);
            return categoryEntity;
        }).sorted((menu1, menu2) -> {
            return (menu1.getSort() == null ? 0: menu1.getSort()) - (menu2.getSort() == null ? 0: menu2.getSort());
        }).collect(Collectors.toList());
        // set到实体类中
        item.setChildren(categoryEntities2);
    }
}
