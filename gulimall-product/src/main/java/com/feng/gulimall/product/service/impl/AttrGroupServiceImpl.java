package com.feng.gulimall.product.service.impl;

import com.alibaba.nacos.shaded.io.grpc.netty.shaded.io.netty.util.internal.StringUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.feng.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.feng.gulimall.product.entity.AttrEntity;
import com.feng.gulimall.product.service.AttrAttrgroupRelationService;
import com.feng.gulimall.product.service.AttrService;
import com.feng.gulimall.product.vo.AttrGroupWithAttrsVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feng.common.utils.PageUtils;
import com.feng.common.utils.Query;

import com.feng.gulimall.product.dao.AttrGroupDao;
import com.feng.gulimall.product.entity.AttrGroupEntity;
import com.feng.gulimall.product.service.AttrGroupService;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    @Autowired
    private AttrService attrService;


    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long catelogId) {
        String key = (String) params.get("key");
        // catelogId默认为零,查询所有
        if (catelogId == 0){
            LambdaQueryWrapper<AttrGroupEntity> wrapper = new LambdaQueryWrapper<>();
            IPage<AttrGroupEntity> page = this.page(
                    new Query<AttrGroupEntity>().getPage(params),
                    queryLike(wrapper, key)
            );
            return new PageUtils(page);
        }else {
            // select * from pms_attr_group where catelog_id = 1 and (attr_group_id = 1 or attr_group_name like %%) order by sort
            LambdaQueryWrapper<AttrGroupEntity> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(AttrGroupEntity::getCatelogId, catelogId);
            IPage<AttrGroupEntity> page = this.page(
                    new Query<AttrGroupEntity>().getPage(params),
                    queryLike(wrapper, key)
            );

            return new PageUtils(page);
        }
    }

    @Override
    public List<AttrGroupWithAttrsVo> getAttrGroupWithAttrs(Long catelogId) {
        // 查出分类信息
        List<AttrGroupEntity> attrGroupEntities = baseMapper.selectList(new LambdaQueryWrapper<AttrGroupEntity>().eq(AttrGroupEntity::getCatelogId, catelogId));
        // 通过关联关系查出分类信息
        List<AttrGroupWithAttrsVo> attrGroupWithAttrsVos = attrGroupEntities.stream().map((attrGroupEntity) -> {
            AttrGroupWithAttrsVo attrGroupWithAttrsVo = new AttrGroupWithAttrsVo();
            BeanUtils.copyProperties(attrGroupEntity, attrGroupWithAttrsVo);
            List<AttrAttrgroupRelationEntity> attrAttrgroupRelationEntities = attrAttrgroupRelationService.getBaseMapper().selectList(new LambdaQueryWrapper<AttrAttrgroupRelationEntity>()
                    .eq(AttrAttrgroupRelationEntity::getAttrGroupId, attrGroupWithAttrsVo.getAttrGroupId()));
            List<AttrEntity> attrEntityList = attrAttrgroupRelationEntities.stream().map((attrAttrgroupRelationEntity) -> {
                return attrService.getById(attrAttrgroupRelationEntity.getAttrId());
            }).collect(Collectors.toList());
            attrGroupWithAttrsVo.setAttrs(attrEntityList);
            return attrGroupWithAttrsVo;
        }).collect(Collectors.toList());
        return attrGroupWithAttrsVos;
    }

    private LambdaQueryWrapper<AttrGroupEntity> queryLike(LambdaQueryWrapper<AttrGroupEntity> wrapper, String key){
        if (key != null && !key.equals("")){
            wrapper.and((obj) -> {
                obj.eq(!StringUtil.isNullOrEmpty(key), AttrGroupEntity::getAttrGroupId, key).or()
                        .like(!StringUtil.isNullOrEmpty(key), AttrGroupEntity::getAttrGroupName, key);
            });
        }
        return wrapper.orderByAsc(AttrGroupEntity::getSort);
    }

}
