package com.feng.gulimall.product.dao;

import com.feng.gulimall.product.entity.AttrGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 属性分组
 * 
 * @author 丰
 * @email sunlightcs@gmail.com
 * @date 2022-10-21 19:58:58
 */
@Mapper
public interface AttrGroupDao extends BaseMapper<AttrGroupEntity> {
	
}
