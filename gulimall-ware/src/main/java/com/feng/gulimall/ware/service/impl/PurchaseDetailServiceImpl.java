package com.feng.gulimall.ware.service.impl;

import com.alibaba.nacos.api.utils.StringUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.feng.common.constant.WareConstant;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.feng.common.utils.PageUtils;
import com.feng.common.utils.Query;

import com.feng.gulimall.ware.dao.PurchaseDetailDao;
import com.feng.gulimall.ware.entity.PurchaseDetailEntity;
import com.feng.gulimall.ware.service.PurchaseDetailService;


@Service("purchaseDetailService")
public class PurchaseDetailServiceImpl extends ServiceImpl<PurchaseDetailDao, PurchaseDetailEntity> implements PurchaseDetailService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        String status = (String) params.get("status");
        String wareId = (String) params.get("wareId");

        LambdaQueryWrapper<PurchaseDetailEntity> wrapper = new LambdaQueryWrapper<>();
        if (!StringUtils.isBlank(key)){
            wrapper.and((w) -> {
                w.eq(PurchaseDetailEntity::getId, key)
                        .or().eq(PurchaseDetailEntity::getSkuId, key)
                        .or().eq(PurchaseDetailEntity::getPurchaseId, key);
            });
        }
        if (!StringUtils.isBlank(status)){
            wrapper.eq(PurchaseDetailEntity::getStatus, status);
        }
        if (!StringUtils.isBlank(wareId)){
            wrapper.eq(PurchaseDetailEntity::getWareId, wareId);
        }

        IPage<PurchaseDetailEntity> page = this.page(
                new Query<PurchaseDetailEntity>().getPage(params),
                wrapper
        );

        return new PageUtils(page);
    }

    @Override
    public void updateDetail(Long id) {
        List<PurchaseDetailEntity> purchaseDetailEntities = baseMapper.selectList(new LambdaQueryWrapper<PurchaseDetailEntity>()
                .eq(PurchaseDetailEntity::getPurchaseId, id));
        // 更新状态
        purchaseDetailEntities.stream().forEach((purchaseDetailEntity) -> {
            PurchaseDetailEntity purchaseDetail = new PurchaseDetailEntity();
            purchaseDetail.setStatus(WareConstant.PurchaseItemStatusEnum.ACCEPT.getCode());
            purchaseDetail.setId(purchaseDetailEntity.getId());
            this.updateById(purchaseDetail);
        });

    }

}
