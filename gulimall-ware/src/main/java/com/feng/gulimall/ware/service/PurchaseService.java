package com.feng.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.feng.common.utils.PageUtils;
import com.feng.gulimall.ware.entity.PurchaseEntity;
import com.feng.gulimall.ware.vo.MergeVo;
import com.feng.gulimall.ware.vo.PurchaseDoneVo;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author 丰
 * @email 1507298022@qq.com
 * @date 2022-10-21 22:02:49
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnreceive(Map<String, Object> params);

    void mergePurchaseItems(MergeVo mergeVo);

    void receive(List<Long> ids);

    void done(PurchaseDoneVo purchaseDoneVo);
}

