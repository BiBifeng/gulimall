package com.feng.gulimall.ware.vo;

import lombok.Data;

/**
 * @description: TODO
 * @author 丰
 * @date 2022/11/18 9:54
 * @version 1.0
 */
@Data
public class PurchaseItemDoneVo {
    private Long itemId;   //
    private Integer status;   // 状态
    private String reason;   // 成功/失败的原因
}
