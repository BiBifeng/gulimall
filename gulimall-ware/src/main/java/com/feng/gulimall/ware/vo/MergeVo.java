package com.feng.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @description:
 * @author 丰
 * @date 2022/11/17 22:24
 * @version 1.0
 */
@Data
public class MergeVo {
    // items
    //:
    //[1, 2]
    //purchaseId
    //:
    //1

    private Long purchaseId;
    private List<Long> items;
}
