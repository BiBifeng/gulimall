package com.feng.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @description: TODO
 * @author 丰
 * @date 2022/11/18 9:53
 * @version 1.0
 */
@Data
public class PurchaseDoneVo {
    private Long id;    // 采购单id
    private List<PurchaseItemDoneVo> items;
}
