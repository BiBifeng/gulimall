package com.feng.gulimall.ware.dao;

import com.feng.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author 丰
 * @email 1507298022@qq.com
 * @date 2022-10-21 22:02:49
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
