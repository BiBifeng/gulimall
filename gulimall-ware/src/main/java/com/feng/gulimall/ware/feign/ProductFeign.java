package com.feng.gulimall.ware.feign;

import com.feng.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @projectName: gulimall
 * @package: com.feng.gulimall.ware.feign
 * @className: ProductFeign
 * @author: 丰
 * @description:
 * @date: 2022/11/18 10:53
 * @version: 1.0
 */
@FeignClient("gulimall-product")
public interface ProductFeign {

    @RequestMapping("/product/skuinfo/info/{skuId}")
    R info(@PathVariable("skuId") Long skuId);

}
