package com.feng.gulimall.ware.dao;

import com.feng.gulimall.ware.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author 丰
 * @email 1507298022@qq.com
 * @date 2022-10-21 22:02:48
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
